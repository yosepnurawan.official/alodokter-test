@extends('layouts.app')

@section('title', 'Create Jenis Surat')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @include('components.alert-info')

            @include('components.alert-error')

            <div class="card">
                <div class="card-header">{{ __('Create Jenis Surat') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('jenis-surat.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="kode_surat" class="col-md-4 col-form-label text-md-right">{{ __('Kode Surat') }}</label>

                            <div class="col-md-6">
                                <input id="kode_surat" type="text" class="form-control @error('kode_surat') is-invalid @enderror" name="kode_surat" value="{{ old('kode_surat') }}" required autocomplete="kode_surat" autofocus>

                                @error('kode_surat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jenis_surat" class="col-md-4 col-form-label text-md-right">{{ __('Jenis Surat') }}</label>

                            <div class="col-md-6">
                                <input id="jenis_surat" type="text" class="form-control @error('jenis_surat') is-invalid @enderror" name="jenis_surat" value="{{ old('jenis_surat') }}" required autocomplete="jenis_surat" autofocus>

                                @error('jenis_surat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                                <a href="{{ route('jenis-surat.index') }}">
                                <button type="button" class="btn btn-danger">
                                    {{ __('Cancel') }}
                                </button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    
@endsection
