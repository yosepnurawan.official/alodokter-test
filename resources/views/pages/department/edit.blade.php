@extends('layouts.app')

@section('title', 'Edit Department')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @include('components.alert-info')

            @include('components.alert-error')

            <div class="card">
                <div class="card-header">{{ __('Edit Department') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('department.update', \Crypt::encrypt($department->id)) }}">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">

                        <div class="form-group row">
                            <label for="kode_department" class="col-md-4 col-form-label text-md-right">{{ __('Kode Department') }}</label>

                            <div class="col-md-6">
                                <input id="kode_department" type="text" class="form-control @error('kode_department') is-invalid @enderror" name="kode_department" value="{{ old('kode_department', $department->kode_department) }}" required autocomplete="kode_department" autofocus>

                                @error('kode_department')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nama_department" class="col-md-4 col-form-label text-md-right">{{ __('Nama Department') }}</label>

                            <div class="col-md-6">
                                <input id="nama_department" type="text" class="form-control @error('nama_department') is-invalid @enderror" name="nama_department" value="{{ old('nama_department',$department->nama_department) }}" required autocomplete="nama_department" autofocus>

                                @error('nama_department')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                                <a href="{{ route('department.index') }}">
                                <button type="button" class="btn btn-danger">
                                    {{ __('Cancel') }}
                                </button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    
@endsection
