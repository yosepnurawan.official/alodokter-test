<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Department;
use App\Models\JenisSurat;
use App\Models\NomorSurat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NomorSuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listNomor = NomorSurat::with('department', 'jenis_surat')->get();

        return view('pages.nomor-surat.index', compact('listNomor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listDepartment = Department::get();
        $listJenis = JenisSurat::get();
        $tahun = date('Y');
        $nomorSuratCount = NomorSurat::get()->count();
        $squence = sprintf("%04d", $nomorSuratCount + 1);

        return view('pages.nomor-surat.create', compact('tahun', 'listJenis', 'listDepartment', 'squence'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tahun' => 'required',
            'department_id' => 'required',
            'jenis_surat_id' => 'required',
            'nomor_surat' => 'required',
        ]);

        DB::beginTransaction();
        try {

            $department = Department::where('kode_department', $request->department_id)->first();
            $jenisSurat = JenisSurat::where('kode_surat', $request->jenis_surat_id)->first();

            NomorSurat::create([
                'tahun' => $request->tahun,
                'department_id' => $department->id,
                'jenis_surat_id' => $jenisSurat->id,
                'nomor_surat' => $request->nomor_surat,
            ]);
            
            DB::commit();
            return redirect(route('nomor-surat.index'))->with('info', 'berhasil di proses');
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect()->back()->withInput()->withErrors('gagal input data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idNomor = \Crypt::decrypt($id);
        $nomorSurat = NomorSurat::findorfail($idNomor);

        $listDepartment = Department::get();
        $listJenis = JenisSurat::get();
        $squence = substr($nomorSurat->nomor_surat, -4);
        
        return view('pages.nomor-surat.edit', compact('nomorSurat', 'listDepartment', 'listJenis', 'squence'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tahun' => 'required',
            'department_id' => 'required',
            'jenis_surat_id' => 'required',
            'nomor_surat' => 'required',
        ]);

        DB::beginTransaction();
        try {

            $department = Department::where('kode_department', $request->department_id)->first();
            $jenisSurat = JenisSurat::where('kode_surat', $request->jenis_surat_id)->first();

            $idNomor = \Crypt::decrypt($id);
            $nomorSurat = NomorSurat::findorfail($idNomor);
            $nomorSurat->tahun = $request->tahun;
            $nomorSurat->department_id = $department->id;
            $nomorSurat->jenis_surat_id = $jenisSurat->id;
            $nomorSurat->nomor_surat = $request->nomor_surat;
            $nomorSurat->save();
            
            DB::commit();
            return redirect(route('nomor-surat.index'))->with('info', 'berhasil di proses');
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect()->back()->withInput()->withErrors('gagal input data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idNomor = \Crypt::decrypt($id);
        $nomorSurat = NomorSurat::findorfail($idNomor);
        $nomorSurat->delete();

        return redirect(route('nomor-surat.index'))->with('info', 'berhasil di delete');
    }
}
