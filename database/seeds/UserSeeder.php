<?php

use App\Models\User;
use App\Models\Department;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department = Department::inRandomOrder()->first();
        $admin = User::create([
            'name' => 'user1',
            'email' => 'user1@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('12345678'),
            'roles' => 'admin',
            'department_id' => $department->id,
        ]);
        
        $departmentOthers = Department::inRandomOrder()->first();
        $user = User::create([
            'name' => 'user2',
            'email' => 'user2@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('12345678'),
            'roles' => 'user',
            'department_id' => $departmentOthers->id,
        ]);
    }
}
