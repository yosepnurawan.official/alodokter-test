@extends('layouts.app')

@section('title', 'User')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @include('components.alert-info')

            @include('components.alert-error')

            <div class="card">
                <div class="card-header">{{ __('User') }}</div>

                <div class="card-body">
                    <table id="user" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Department</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $listUser as $item)
                            <tr>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->department->nama_department }}</td>
                                <td>
                                    <form method="POST" action="{{ route('user.destroy', \Crypt::encrypt($item->id)) }}">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <div class="form-group row mb-0">
                                        <div class="col-md-12">
                                            <a href="{{ route('user.edit', \Crypt::encrypt($item->id)) }}">
                                                <button type="button" class="btn btn-primary">
                                                    {{ __('Edit') }}
                                                </button>
                                                </a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('anda yakin mau delete?')">
                                                {{ __('Delete') }}
                                            </button>
                                        </div>
                                    </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Department</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ URL::asset('js/user.js') }}"></script>
@endsection
