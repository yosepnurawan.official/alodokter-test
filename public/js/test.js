(function ($) {

    $(document).on("click", "#add" , function() {
        addRow()
    });

    $(document).on("click", "#del" , function() {
        delRow(this)
    });

    function addRow() {
        let newRow = $("<div id='container'>")
        let cols = ``

        cols += `<hr/>
                <div class="form-group row">
                    <label for="files" class="col-md-4 col-form-label text-md-right">File</label>

                    <div class="col-md-6">
                        <input id="files" type="file" class="form-control" name="files[]" value="" autofocus>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="keterangan" class="col-md-4 col-form-label text-md-right">Keterangan</label>

                    <div class="col-md-3">
                        <input id="keterangan" type="text" class="form-control" name="keterangan[]" value="" required autocomplete="name" autofocus>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-danger delete" id="del">Hapus Row</button>
                    </div>
                </div>
                <hr/>`

        newRow.append(cols).hide().show('slow')
        $("#append-code").append(newRow)
    }

    function delRow(e) {
        $(e).closest("#container").remove()
    }
    
})(jQuery)