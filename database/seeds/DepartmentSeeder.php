<?php

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listDepartment = [
            'IT' => 'Information Technology',
            'FIN' => 'Finance',
            'GA' => 'General Affair',
        ];

        foreach ($listDepartment as $key => $value) {
            Department::create([
                'kode_department' => $key,
		        'nama_department' => $value,
            ]);
        }
    }
}
