<?php

namespace App\Http\Controllers;

use DB;
use App\Models\JenisSurat;
use Illuminate\Http\Request;

class JenisSuratController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listJenis = JenisSurat::get();

        return view('pages.jenis-surat.index', compact('listJenis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.jenis-surat.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_surat' => 'required',
            'jenis_surat' => 'required',
        ]);

        DB::beginTransaction();
        try {

            JenisSurat::create([
                'kode_surat' => $request->kode_surat,
                'jenis_surat' => $request->jenis_surat,
            ]);
            
            DB::commit();
            return redirect(route('jenis-surat.index'))->with('info', 'berhasil di proses');
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect()->back()->withInput()->withErrors('gagal input data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idJenis = \Crypt::decrypt($id);
        $jenisSurat = JenisSurat::findorfail($idJenis);
        
        return view('pages.jenis-surat.edit', compact('jenisSurat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode_surat' => 'required',
            'jenis_surat' => 'required',
        ]);

        DB::beginTransaction();
        try {

            $idJenis = \Crypt::decrypt($id);
            $jenisSurat = JenisSurat::findorfail($idJenis);

            $jenisSurat->kode_surat = $request->kode_surat;
            $jenisSurat->jenis_surat = $request->jenis_surat;
            $jenisSurat->save();
            
            DB::commit();
            return redirect(route('jenis-surat.index'))->with('info', 'berhasil di update');
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect()->back()->withInput()->withErrors('gagal input data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idJenis = \Crypt::decrypt($id);
        $jenisSurat = JenisSurat::findorfail($idJenis);

        $jenisSurat->delete();

        return redirect(route('jenis-surat.index'))->with('info', 'berhasil di delete');
    }
}
