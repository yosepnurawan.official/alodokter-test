<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listDepartment = Department::get();

        return view('pages.department.index', compact('listDepartment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_department' => 'required',
            'nama_department' => 'required',
        ]);

        DB::beginTransaction();
        try {

            Department::create([
                'kode_department' => $request->kode_department,
                'nama_department' => $request->nama_department,
            ]);
            
            DB::commit();
            return redirect(route('department.index'))->with('info', 'berhasil di proses');
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect()->back()->withInput()->withErrors('gagal input data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idDepartment = \Crypt::decrypt($id);
        $department = Department::findorfail($idDepartment);
        
        return view('pages.department.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode_department' => 'required',
            'nama_department' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $idDepartment = \Crypt::decrypt($id);
            $department = Department::findorfail($idDepartment);
            $department->kode_department = $request->kode_department;
            $department->nama_department = $request->nama_department;
            $department->save();
            
            DB::commit();
            return redirect(route('department.index'))->with('info', 'berhasil di update');
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect()->back()->withInput()->withErrors('gagal input data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idDepartment = \Crypt::decrypt($id);
        $department = Department::findorfail($idDepartment);
        $department->delete();

        return redirect(route('department.index'))->with('info', 'berhasil di delete');
    }
}
