<?php

namespace App\Http\Controllers;

use DB;
use App\Models\User;
use App\Models\Department;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listUser = User::with('department')->get();

        return view('pages.user.index', compact('listUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $idUser = \Crypt::decrypt($id);
        $user = User::findorfail($idUser);
        $listDepartment = Department::get();

        return view('pages.user.edit', compact('user', 'listDepartment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'department' => 'required',
        ]);

        DB::beginTransaction();
        try {

            $idUser = \Crypt::decrypt($id);
            $user = User::findorfail($idUser);

            $user->name = $request->name;
            $user->email = $request->email;
            $user->department_id = $request->department;
            $user->save();
            
            DB::commit();
            return redirect(route('user.index'))->with('info', 'berhasil di update');
        } catch (\Throwable $th) {
            //throw $th;
            DB::rollback();
            return redirect()->back()->withInput()->withErrors('gagal input data');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idUser = \Crypt::decrypt($id);
        $user = User::findorfail($idUser);

        $user->delete();

        return redirect(route('user.index'))->with('info', 'berhasil di delete');
    }
}
