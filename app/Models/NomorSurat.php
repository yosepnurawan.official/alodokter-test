<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NomorSurat
 * 
 * @property int $id
 * @property string $tahun
 * @property int $department_id
 * @property int $jenis_surat_id
 * @property string $nomor_surat
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Department $department
 * @property JenisSurat $jenis_surat
 *
 * @package App\Models
 */
class NomorSurat extends Model
{
	protected $table = 'nomor_surats';

	protected $casts = [
		'department_id' => 'int',
		'jenis_surat_id' => 'int'
	];

	protected $fillable = [
		'tahun',
		'department_id',
		'jenis_surat_id',
		'nomor_surat'
	];

	public function department()
	{
		return $this->belongsTo(Department::class);
	}

	public function jenis_surat()
	{
		return $this->belongsTo(JenisSurat::class);
	}
}
