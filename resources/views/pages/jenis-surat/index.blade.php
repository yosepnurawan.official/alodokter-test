@extends('layouts.app')

@section('title', 'Jenis Surat')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @include('components.alert-info')

            @include('components.alert-error')

            <div class="card">
                <div class="card-header">{{ __('Jenis Surat') }}</div>

                <div class="card-body">
                    <div class="form-group row mb-4">
                        <div class="col-md-6">
                            <a href="{{ route('jenis-surat.create') }}">
                            <button type="button" class="btn btn-primary">
                                {{ __('Input Data') }}
                            </button>
                            </a>
                        </div>
                    </div>
                    <table id="jenis-surat" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Kode Surat</th>
                                <th>Jenis Surat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $listJenis as $item)
                            <tr>
                                <td>{{ $item->kode_surat }}</td>
                                <td>{{ $item->jenis_surat }}</td>
                                <td>
                                    <form method="POST" action="{{ route('jenis-surat.destroy', \Crypt::encrypt($item->id)) }}">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <div class="form-group row mb-0">
                                        <div class="col-md-12">
                                            <a href="{{ route('jenis-surat.edit', \Crypt::encrypt($item->id)) }}">
                                                <button type="button" class="btn btn-primary">
                                                    {{ __('Edit') }}
                                                </button>
                                                </a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('anda yakin mau delete?')">
                                                {{ __('Delete') }}
                                            </button>
                                        </div>
                                    </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Kode Surat</th>
                                <th>Jenis Surat</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ URL::asset('js/jenis-surat.js') }}"></script>
@endsection
