(function ($) {
    const squence = $('#squence').val()
    const tahun = $('#tahun').val()

    $(document).ready(function() {
        $( "#department_id" ).change(function() {
            formatLetterNumber($(this).val(), 'department_id')
        });

        $( "#jenis_surat_id" ).change(function() {
            formatLetterNumber($(this).val(), 'jenis_surat_id')
        });
    });

    function formatLetterNumber(val, type) {
        let department = ''
        let jenis_surat = ''
        let format = ''

        if (type == "department_id") {
            department = val
            jenis_surat = $('#jenis_surat_id').val()
        }

        if (type == "jenis_surat_id") {
            jenis_surat = val
            department = $('#department_id').val()
        }

        format = tahun + '/' + department + '-' + jenis_surat + '/' + squence

        $('#nomor_surat').val(format)
    }
})(jQuery)