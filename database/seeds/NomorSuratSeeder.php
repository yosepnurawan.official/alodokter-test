<?php

use App\Models\Department;
use App\Models\JenisSurat;
use App\Models\NomorSurat;
use Illuminate\Database\Seeder;

class NomorSuratSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 4; $i++) { 
            $department = Department::inRandomOrder()->first();
            $jenisSurat = JenisSurat::inRandomOrder()->first();
            $tahun = date('Y');

            $nomorSuratCount = NomorSurat::get()->count();
            $squence = sprintf("%04d", $nomorSuratCount + 1);
            $format = $tahun . '/' . $department->kode_department . '-' . $jenisSurat->kode_surat . '/' . $squence;

            NomorSurat::create([
                'tahun' => $tahun,
                'department_id' => $department->id,
                'jenis_surat_id' => $jenisSurat->id,
                'nomor_surat' => $format,
            ]);
        }
    }
}
