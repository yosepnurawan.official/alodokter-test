<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class JenisSurat
 * 
 * @property int $id
 * @property string $kode_surat
 * @property string $jenis_surat
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class JenisSurat extends Model
{
	protected $table = 'jenis_surats';

	protected $fillable = [
		'kode_surat',
		'jenis_surat'
	];
}
