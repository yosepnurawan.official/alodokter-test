@extends('layouts.app')

@section('title', 'Edit Nomor Surat')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @include('components.alert-info')

            @include('components.alert-error')

            <div class="card">
                <div class="card-header">{{ __('Edit Nomor Surat') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('nomor-surat.update', \Crypt::encrypt($nomorSurat->id)) }}">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="squence" id="squence" value="{{ $squence }}">

                        <div class="form-group row">
                            <label for="tahun" class="col-md-4 col-form-label text-md-right">{{ __('Tahun') }}</label>

                            <div class="col-md-6">
                                <input id="tahun" type="text" class="form-control @error('tahun') is-invalid @enderror" name="tahun" value="{{ old('tahun', $nomorSurat->tahun) }}" required autocomplete="tahun" autofocus readonly>

                                @error('tahun')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="department_id" class="col-md-4 col-form-label text-md-right">{{ __('Nama Department') }}</label>

                            <div class="col-md-6">

                                <select id="department_id" class="form-control @error('department_id') is-invalid @enderror" name="department_id">
                                    <option value="">-- Pilih --</option>
                                    @foreach ( $listDepartment as $dep )
                                    @php
                                        $selected = ($dep->id == $nomorSurat->department_id) ? 'selected' : '';
                                    @endphp
                                    <option value="{{ $dep->kode_department }}" {{ $selected }}>{{ $dep->nama_department }}</option>
                                    @endforeach
                                </select>

                                @error('department_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="jenis_surat_id" class="col-md-4 col-form-label text-md-right">{{ __('Jenis Surat') }}</label>

                            <div class="col-md-6">
                                <select id="jenis_surat_id" class="form-control @error('jenis_surat_id') is-invalid @enderror" name="jenis_surat_id">
                                    <option value="">-- Pilih --</option>
                                    @foreach ( $listJenis as $dep )
                                    @php
                                        $selected = ($dep->id == $nomorSurat->jenis_surat_id) ? 'selected' : '';
                                    @endphp
                                    <option value="{{ $dep->kode_surat }}" {{ $selected }}>{{ $dep->jenis_surat }}</option>
                                    @endforeach
                                </select>

                                @error('jenis_surat_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nomor_surat" class="col-md-4 col-form-label text-md-right">{{ __('Nomor Surat') }}</label>

                            <div class="col-md-6">
                                <input id="nomor_surat" type="text" class="form-control @error('nomor_surat') is-invalid @enderror" name="nomor_surat" value="{{ old('nomor_surat', $nomorSurat->nomor_surat) }}" required autocomplete="nomor_surat" autofocus readonly>

                                @error('nomor_surat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                                <a href="{{ route('nomor-surat.index') }}">
                                <button type="button" class="btn btn-danger">
                                    {{ __('Cancel') }}
                                </button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ URL::asset('js/input-nomor.js') }}"></script>
@endsection
