<?php

use App\Models\JenisSurat;
use Illuminate\Database\Seeder;

class JenisSuratSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $listJenis = [
            'Surat Perintah',
            'Surat Tugas',
            'Surat Mutasi',
        ];

        foreach ($listJenis as $key => $value) {
            $squence = sprintf('%03d', $key + 1);
            JenisSurat::create([
                'kode_surat' => $squence,
		        'jenis_surat' => $value,
            ]);
        }
    }
}
