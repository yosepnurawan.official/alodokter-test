@extends('layouts.app')

@section('title', 'Nomor Surat')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @include('components.alert-info')

            @include('components.alert-error')

            <div class="card">
                <div class="card-header">{{ __('Nomor Surat') }}</div>

                <div class="card-body">
                    <div class="form-group row mb-4">
                        <div class="col-md-6">
                            <a href="{{ route('nomor-surat.create') }}">
                            <button type="button" class="btn btn-primary">
                                {{ __('Input Data') }}
                            </button>
                            </a>
                        </div>
                    </div>

                    <table id="nomor-surat" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Tahun</th>
                                <th>Nama Department</th>
                                <th>Jenis Surat</th>
                                <th>Nomor Surat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $listNomor as $item)
                            <tr>
                                <td>{{ $item->tahun }}</td>
                                <td>{{ $item->department->nama_department }}</td>
                                <td>{{ $item->jenis_surat->jenis_surat }}</td>
                                <td>{{ $item->nomor_surat }}</td>
                                <td>
                                    <form method="POST" action="{{ route('nomor-surat.destroy', \Crypt::encrypt($item->id)) }}">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <div class="form-group row mb-0">
                                        <div class="col-md-12">
                                            <a href="{{ route('nomor-surat.edit', \Crypt::encrypt($item->id)) }}">
                                            <button type="button" class="btn btn-primary">
                                                {{ __('Edit') }}
                                            </button>
                                            </a>
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('anda yakin mau delete?')">
                                                {{ __('Delete') }}
                                            </button>
                                        </div>
                                    </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Tahun</th>
                                <th>Nama Department</th>
                                <th>Jenis Surat</th>
                                <th>Nomor Surat</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ URL::asset('js/department.js') }}"></script>
@endsection
